class Temperature
  attr_accessor :f, :c

  def self.from_celsius(c)
    self.new(:c => c)
  end

  def self.from_fahrenheit(f)
    self.new(:f => f)
  end

  def initialize(options = {})
    @f = options[:f]
    @c = options[:c]
  end

  def in_celsius
    @c || ftoc(@f)
  end

  def in_fahrenheit
    @f || ctof(@c)
  end

  def ftoc(f)
    (f - 32.0) * (5.0 / 9.0)
  end

  def ctof(c)
    c * (9.0 / 5.0) + 32.0
  end
end

class Celsius < Temperature
  attr_accessor :c

  def initialize(c)
    @c = c
  end
end

class Fahrenheit < Temperature
  attr_accessor :f

  def initialize(f)
    @f = f
  end
end
