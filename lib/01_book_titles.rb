class Book
  def title
    @title
  end

  def title=(new_title)
    @title = titleize(new_title)
  end

  def titleize(title)
    dont_capitalize = ["the", "a", "an", "and", "in", "of"]
    title.split(' ').map.with_index(0) do |word, idx|
      if dont_capitalize.include?(word) && idx > 0
        word
      else
        word.capitalize
      end
    end.join(' ')
  end
end
