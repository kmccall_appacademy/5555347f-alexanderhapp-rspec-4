class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = @seconds / 60 - hours * 60
    seconds = @seconds - (minutes * 60 + hours * 3600)
    delimiter = ":"
    padded(hours) + delimiter + padded(minutes) + delimiter + padded(seconds)
  end

  def padded(number)
    string_num = number.to_s
    string_num.length < 2 ? "0#{string_num}" : string_num
  end
end
