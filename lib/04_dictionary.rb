class Dictionary
  attr_accessor :entries

  def initialize(entries = {})
    @entries = entries
  end

  def add(var)
    var.is_a?(String) ? entries.merge!(var => nil) : entries.merge!(var)
  end

  def keywords
    entries.keys.sort
  end

  def include?(key)
    entries.key?(key)
  end

  def find(string)
    if string.length == 2
      entries.select { |key| key.match(/\b#{string}/) }
    else
      entries.select { |key| key == string }
    end
  end

  def printable
    printable_value = ''
    entries.sort.each do |k,v|
      printable_value += %{[#{k}] "#{v}"\n}
    end
    printable_value.chop
  end
end
